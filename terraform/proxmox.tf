variable pm_api_url {}
variable pm_user {}
variable pm_password {}
variable pm_node {}

provider "proxmox" {
  pm_api_url = var.pm_api_url
  pm_user = var.pm_user
  pm_password = var.pm_password
}

resource "proxmox_vm_qemu" "sample-system" {
  name = "terra-test"
  target_node = var.pm_node
  clone = "rhel8-template"
  full_clone = "true"
  memory = 2048
  disk {
    id = 0
    size = 32
    type = "virtio"
    storage = "local-lvm"
  }
  network {
    id = 0
    model = "virtio"
    bridge = "vmbr0"
  }
}


